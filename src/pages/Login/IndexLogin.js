import React from 'react';
import './Login.css';
import { Input, Row, Col,Button, Form, Icon, Checkbox  } from 'antd';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

function hasErrors(fieldsError) {
  return Object.keys(fieldsError).some(field => fieldsError[field]);
}

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    // To disabled submit button at the beginning.
    this.props.form.validateFields();
  }

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const DemoBox = props => <p className={`height-${props.value}`}>{props.children}</p>;

    return (
      <div >
      
        <div className="row" style={{ paddingTop: '150px',}}>
        <Row  align="bottom" >
          <Col span={8} offset={8}>
            <Col span={8} offset={5}>
            <img style={{ paddingBottom:'10px',height:'100%'}} src='https://m.danain.co.id/gambar/logo-danain.jpg' alt='logo-danain'/>
            </Col>
           
            <Form onSubmit={this.handleSubmit} className="login-form">
              <Form.Item>
                {getFieldDecorator('username', {
                  rules: [{ required: true, message: 'Please input your username!' }],
                })(
                  <Input
                    prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                    placeholder="Username"
                  />,
                )}
              </Form.Item>
              <Form.Item>
                {getFieldDecorator('password', {
                  rules: [{ required: true, message: 'Please input your Password!' }],
                })(
                  <Input
                    prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                    type="password"
                    placeholder="Password"
                  />,
                )}
              </Form.Item>
              <Form.Item>
                
                
                <Button type="primary" htmlType="submit" className="login-form-button">
                  Log in
                </Button>
              
              </Form.Item>
            </Form>
          </Col>
          
        </Row>
          
        </div>
        {/* <Row>
          <Col span={6} offset={9}>
          <center><img src='https://m.danain.co.id/gambar/logo-danain.jpg' alt='logo-danain'/></center>
          <br />
            <Input placeholder="Email" />
            <br />
            <br />
            <Input.Password placeholder="Password" />
            <br />
            <br />
            <Router>
            <Link to="/pages/IndexDashboard"><Button type="primary" >Login</Button></Link> &nbsp;&nbsp;
            <Link to="register"><Button type="danger" >Register</Button></Link>
            </Router>
            &nbsp;&nbsp;&nbsp;
          </Col>
        </Row> */}
      </div>
    );
  }
}

//export default App;
export default Form.create()(App);
