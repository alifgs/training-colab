import React from 'react'
import Login from "./pages/Login/IndexLogin";
import Register from "./pages/register/IndexRegister";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    Redirect
  } from "react-router-dom";
import IndexRegister from './pages/register/IndexRegister';


class Root extends React.Component {
    render() {
        return (
            <div>
                <Router>
                    <Switch>
                        <Redirect exact from="/" to="/register" />
                        <Route exact path="/login" component={Login} />
                        <Route exact path="/register" component={Register} />
                        
                    </Switch>
                </Router>
            </div>
            
        );
    }
}
export default Root